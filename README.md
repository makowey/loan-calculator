![Logo](https://bytebucket.org/makowey/loan-calculator/raw/0f3d26a099b6ba6d5c22281ace6b0fe22f8bfc79/src/main/resources/static/logo.PNG)
 
>Create a simple application which can be used for calculation of the cost from a housing loan.
The application should have a simple user interface where the user can specify the desired amount 
and the payback time in years. 
 
>For simplicity we assume a fixed interest of 3.5% per year during the complete payback time. 
The interest should be connected to the loan type in such a manner that different loan types 
can have different interests. 
 
>When selecting amount and payback time, the application should generate a monthly payback plan 
based on the series loan principle, i.e. you pay back an equal amount each month and add the generated interest. 
The interest is calculated every month.
 
>The application should be made in such a manner that it can easily be extended to calculate a
payment plan for other types of loans, for instance car loan, spending loan etc. with different
interests. Also bear in mind that it should be easy to extend the application to cover other
payback schemes as well. We do not expect this to be implemented.
 
>We expect that you demonstrate reasonable use of the available language functionality for
abstraction, interfaces, inheritance, a good class structure and show a good programming
practice. 
 
>We are interested to see use of AJAX and Javascript on the webpage. The backend should be developed in Java. 
We are focused on code quality and good design, and a design sketch should be provided.

# User Interface
![Image of Calculator](https://bytebucket.org/makowey/loan-calculator/raw/a640e43f868734d29a28684c6a1b85e2aa6800d9/src/main/resources/static/snapshot.PNG)

# Source code
The source can be downloaded from bitbucket using git command: 
`git clone git@bitbucket.org:makowey/loan-calculator.git`
[Download zip](https://bitbucket.org/makowey/loan-calculator/get/master.zip)

# Design Sketch
Click on the logo app page, [Source](https://www.draw.io/#G1v2iaVOrMZ4DGyeZvIuImojMIa_Ostlqq) || [Shared](https://www.draw.io/?lightbox=1&highlight=0000ff&edit=_blank&layers=1&nav=1&title=Loan%20Calculator.html#Uhttps%3A%2F%2Fdrive.google.com%2Fuc%3Fid%3D1v2iaVOrMZ4DGyeZvIuImojMIa_Ostlqq%26export%3Ddownload) (external access)

# Test
In order to test the application make sure that your are in the project folder, where the **pom.xml** is placed.  
``` cd loan-calculator && mvn clean test```

# Run the application
In order to **run** the application make sure that your are in the project folder, where the **pom.xml** is placed. 

After you run this command  
```mvn spring-boot:run```  
open the browser at [http://localhost:8080](http://localhost:8080)

# DB connection
You can connect, in running mode, to H2 database UI at [http://localhost:8080/h2/](http://localhost:8080/h2/)  
username: _root_  
password: _root_

# Live on HEROKU
Deployed on cloud: [https://loan-simulator.herokuapp.com/](https://loan-simulator.herokuapp.com/)  
DB web access: [https://loan-simulator.herokuapp.com/h2/](https://loan-simulator.herokuapp.com/h2/)  

@makowey