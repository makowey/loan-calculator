package com.makowey.controllers;

import com.makowey.model.ImmutableMonthlyLoanBill;
import com.makowey.model.Interest;
import com.makowey.model.loans.FlexibleLoan;
import com.makowey.model.loans.GenericLoan;
import com.makowey.model.loans.PersonalLoan;
import com.makowey.model.loans.StandardLoan;
import com.makowey.model.type.InterestType;
import com.makowey.service.LoanCalculatorService;
import com.makowey.service.LoanDataService;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@RestController
@Named
public class LoanController {

    @Inject
    private LoanDataService loanDataService;

    @Inject
    private LoanCalculatorService loanCalculatorService;

    @RequestMapping(value = "/loans/{id}",
            method = RequestMethod.GET,
            produces = "application/json"
    )
    public Interest getLoanById(@PathVariable Long id) {
        return loanDataService.getById(id);
    }

    @RequestMapping(value = "/interest",
            method = RequestMethod.GET,
            produces = "application/json"
    )
    public Interest getLoanDetailsByName(@RequestParam String name) {
        return loanDataService.getByName(name);
    }

    @RequestMapping(value = "/loans/",
            method = RequestMethod.GET,
            produces = "application/json"
    )
    public List<Interest> getAll() {
        return loanDataService.getAll();
    }

    @RequestMapping(value = "/plan/{amount}/standard/{years}",
            method = RequestMethod.GET,
            produces = "application/json"
    )
    public List<ImmutableMonthlyLoanBill> generateStandardMonthlyPaybackPlan(
            @PathVariable double amount,
            @PathVariable int years) {
        Interest interest = loanDataService.getByName(InterestType.STANDARD.name());
        GenericLoan loan = new StandardLoan(amount, interest, years);

        return loanCalculatorService.generateMonthlyPaybackPlan(loan);
    }

    @RequestMapping(value = "/plan/{amount}/flexible/{years}",
            method = RequestMethod.GET,
            produces = "application/json"
    )
    public List<ImmutableMonthlyLoanBill> generateFlexibleMonthlyPaybackPlan(
            @PathVariable double amount,
            @PathVariable int years) {
        Interest interest = loanDataService.getByName(InterestType.FLEXIBLE.name());
        GenericLoan loan = new FlexibleLoan(amount, interest, years);

        return loanCalculatorService.generateMonthlyPaybackPlan(loan);
    }

    @RequestMapping(value = "/plan/{amount}/personal/{years}",
            method = RequestMethod.GET,
            produces = "application/json"
    )
    public List<ImmutableMonthlyLoanBill> generatePersonalMonthlyPaybackPlan(
            @PathVariable double amount,
            @PathVariable int years) {
        Interest interest = loanDataService.getByName(InterestType.PERSONAL.name());
        GenericLoan loan = new PersonalLoan(amount, interest, years);

        return loanCalculatorService.generateMonthlyPaybackPlan(loan);
    }
}

