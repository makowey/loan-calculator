package com.makowey.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import static java.lang.String.format;

@Entity
@Table(name = "Interest")
public class Interest {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    @Column(nullable = false)
    private Double value;

    @NotNull
    @Column(nullable = false)
    private String type;

    public Interest() {
    }

    public Interest(@NotNull Double value, @NotNull String type) {
        this.value = value;
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public String getPercent() {
        return format("%.2f%%", value);
    }

    public Double getValue() {
        return value / 100;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return format("%s - %s", getType(), getPercent());
    }

    @Override
    public String toString() {
        return "Interest{" +
                "id=" + id +
                ", value='" + value + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
