package com.makowey.model;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

@Value.Immutable
@JsonSerialize(as = ImmutableMonthlyLoanBill.class)
@JsonDeserialize(as = ImmutableMonthlyLoanBill.class)
public interface MonthlyLoanBill {

    int id();

    String month();

    int year();

    double interest();

    double principal();

    double payment();

    double endingBalance();
}
