package com.makowey.model.loans;

import com.makowey.exceptions.LoanNotFoundException;
import com.makowey.model.ImmutableMonthlyLoanBill;
import com.makowey.model.Interest;

import java.util.List;

public class FlexibleLoan extends GenericLoan {

    public FlexibleLoan(double amount, Interest interest, int years) {
        super(amount, interest, years);
    }

    @Override
    public List<ImmutableMonthlyLoanBill> calculateMonthlyPaybackPlan() {
        throw new LoanNotFoundException("There is no calculateMonthlyPaybackPlan in place for FLEXIBLE!!!");
    }
}
