package com.makowey.model.loans;

import com.makowey.model.ImmutableMonthlyLoanBill;
import com.makowey.model.Interest;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;

public abstract class GenericLoan {

    protected final double amount;

    protected final Interest interest;

    protected final int years;

    public GenericLoan(double amount, Interest interest, int years) {
        this.amount = amount;
        this.interest = interest;
        this.years = years;
    }

    public abstract List<ImmutableMonthlyLoanBill> calculateMonthlyPaybackPlan();

    protected double setPrecision(double value) {
        DecimalFormat df2 = new DecimalFormat(".##");
        df2.setRoundingMode(RoundingMode.HALF_DOWN);
        return Double.valueOf(df2.format(value));
    }
}
