package com.makowey.model.loans;

import com.makowey.model.ImmutableMonthlyLoanBill;
import com.makowey.model.Interest;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

public class PersonalLoan extends GenericLoan {

    private final int ADMINISTRATIVE_COMMISSION = 10;

    public PersonalLoan(double amount, Interest interest, int years) {
        super(amount, interest, years);
    }

    @Override
    public List<ImmutableMonthlyLoanBill> calculateMonthlyPaybackPlan() {
        final int numberOfMonths = Month.values().length;
        final double monthlyAmount = amortizingLoanPayment(amount, interest, years);

        final double period = interest.getValue() / numberOfMonths;
        final double[] amountReference = new double[]{amount};

        LocalDate currentDate = LocalDate.now();
        LocalDate endDate = currentDate.plusYears(years);
        List<ImmutableMonthlyLoanBill> billList = new ArrayList<>();

        int counter = 0;
        while (currentDate.isBefore(endDate)) {
            final int year = currentDate.getYear();

            final double interestAmount = amountReference[0] * period;
            final double principal = monthlyAmount - interestAmount;
            final double endingBalance = amountReference[0] - principal;
            final Month month = currentDate.getMonth();
            amountReference[0] = endingBalance;

            ImmutableMonthlyLoanBill monthlyLoanBill =
                    ImmutableMonthlyLoanBill.builder()
                            .id(++counter)
                            .month(month.name())
                            .interest(setPrecision(interestAmount))
                            .principal(setPrecision(principal))
                            .endingBalance(setPrecision(endingBalance))
                            .payment(setPrecision(monthlyAmount + ADMINISTRATIVE_COMMISSION))
                            .year(year)
                            .build();

            billList.add(monthlyLoanBill);
            currentDate = currentDate.plusMonths(1L);
        }

        return billList;
    }

    private double amortizingLoanPayment(double amount, Interest interestType, int years) {
        int numberOfMonths = Month.values().length;
        double interestRate = interestType.getValue();
        double monthlyRate = interestRate / numberOfMonths;
        int periodInMonth = years * numberOfMonths;

        return (amount * monthlyRate) /
                (1 - Math.pow(1 + monthlyRate, -periodInMonth));
    }
}
