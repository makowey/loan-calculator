package com.makowey.model.type;

public enum InterestType {
    STANDARD,
    FLEXIBLE,
    PERSONAL
}
