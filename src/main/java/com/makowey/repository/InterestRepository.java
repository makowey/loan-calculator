package com.makowey.repository;

import com.makowey.model.Interest;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import java.util.Optional;
import java.util.stream.Stream;

public interface InterestRepository extends Repository<Interest, Long> {

    Optional<Interest> findById(Long id);

    Optional<Interest> findByType(String type);

    @Query("select c from Interest c")
    Stream<Interest> streamAllInterests();
}
