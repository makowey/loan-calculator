package com.makowey.service;

import com.makowey.model.ImmutableMonthlyLoanBill;
import com.makowey.model.loans.GenericLoan;

import java.util.List;

public interface LoanCalculatorService {
    List<ImmutableMonthlyLoanBill> generateMonthlyPaybackPlan(GenericLoan genericLoan);
}
