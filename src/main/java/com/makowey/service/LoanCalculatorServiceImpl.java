package com.makowey.service;

import com.makowey.model.ImmutableMonthlyLoanBill;
import com.makowey.model.loans.GenericLoan;

import javax.inject.Named;
import java.util.List;

@Named
public class LoanCalculatorServiceImpl implements LoanCalculatorService {

    @Override
    public List<ImmutableMonthlyLoanBill> generateMonthlyPaybackPlan(GenericLoan genericLoan) {
        return genericLoan.calculateMonthlyPaybackPlan();
    }
}
