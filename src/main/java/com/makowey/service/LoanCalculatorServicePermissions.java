package com.makowey.service;

import com.makowey.model.ImmutableMonthlyLoanBill;
import com.makowey.model.loans.GenericLoan;
import org.springframework.context.annotation.Primary;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@Named
@Primary
public class LoanCalculatorServicePermissions implements LoanCalculatorService {

    @Inject
    LoanCalculatorService loanCalculatorService;

    @Override
    public List<ImmutableMonthlyLoanBill> generateMonthlyPaybackPlan(GenericLoan genericLoan) {
        System.err.println("Checking permission operation....");
        return loanCalculatorService.generateMonthlyPaybackPlan(genericLoan);
    }
}
