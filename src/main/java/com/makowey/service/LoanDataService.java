package com.makowey.service;

import com.makowey.exceptions.LoanNotFoundException;
import com.makowey.model.Interest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.makowey.repository.InterestRepository;

import java.util.List;
import java.util.stream.Collectors;

import static java.lang.String.format;

@Service
@Transactional(readOnly = true)
public class LoanDataService {

    @Autowired
    InterestRepository interestRepository;

    public LoanDataService(InterestRepository interestRepository) {
        this.interestRepository = interestRepository;
    }

    public Interest getById(Long id) {
        return interestRepository.findById(id)
                .orElseThrow(() -> new LoanNotFoundException(format("Interest %d not found", id)));
    }

    public List<Interest> getAll() {
        return interestRepository.streamAllInterests().collect(Collectors.toList());
    }

    public Interest getByName(String type) {
        return interestRepository.findByType(type.toUpperCase())
                .orElseThrow(() -> new LoanNotFoundException(format("Interest with type `%s` not found", type.toUpperCase())));
    }
}
