var host = ""; //http://localhost:8080";
var loansEndpoint = host.concat("/loans/");
var planEndpoint = host.concat("/plan/");

function populateLoanTypes() {
    $.getJSON(loansEndpoint, function (data) {
        $.each(data, function (i, data) {
            var div_data = "<option value=" + data.type + ">" + data.name + "</option>";
            $(div_data).appendTo('#interest');
        });
    });
}

$(document).ready(function () {
    console.log("ready!");

    populateLoanTypes();

    $("#plan-header").hide();

    $("#loanCalculator").submit(function (event) {
        event.preventDefault();
        updateContent();
    });
});

function populateTable(data) {
    $("#plan").dynatable({
        table: {
            defaultColumnIdStyle: 'camelCase'
        },
        dataset: {
            records: data,
            perPageDefault: 12
        },
        features: {
            sort: false,
            perPageSelect: false,
            pushState: true,
            search: false
        },
        inputs: {
            recordCountTextShowing: 'Showing ',
            recordCountTextTo: ' to ',
            recordCountTextOf: ' of ',
            pages: 'Years: '

        },
        params: {
            records: 'months'
        }
    })
        .data('dynatable');

    $("#plan").data('dynatable').settings.dataset.originalRecords = data;
    $("#plan").data('dynatable').process();

    $("#plan-header").fadeIn();
}

function getPlan(amount, interest, years) {
    $.get( planEndpoint + amount + "/" + interest.toLowerCase() + "/" + years,
        function (data) {

            populateTable(data);

            printStatistics(data, amount, interest, years);

            $("#planDetailed").show();
        })
        .fail(function ($xhr) {
            var data = $xhr.responseJSON;
            $("#message").text(data.message)
                .attr('class', 'alert alert-danger');
            $("#planDetailed").hide();
        });
}

function updateContent() {
    var amount = $("#amount").val();
    var years = $("#years").val();
    var interest = $("#interest").val();

    getPlan(amount, interest, years);
}

function getTotalInterest(data) {
    var totalInterest = 0, i;
    for (i = 0; i < data.length; i++) {
        totalInterest += data[i].interest;
    }

    return totalInterest.toFixed(2);
}

function numberWithSpaces(number) {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
}

function printStatistics(data, amount, interest, years) {
    var totalInterest = getTotalInterest(data);
    var totalDebt = Number(amount) + Number(totalInterest);
    var currency = $("#currency").text();

    $("#message").html("Calculation for " + currency.concat(numberWithSpaces(amount)) + ", " +
        interest + " loan for " + years + " years." +
        "<br>Total of interest: " + currency.concat(numberWithSpaces(totalInterest)) +
        ". You will pay back: " + currency.concat(numberWithSpaces(totalDebt.toFixed(2))))
        .attr('class', 'alert alert-success');
}