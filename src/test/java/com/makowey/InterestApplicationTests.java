package com.makowey;

import com.makowey.controllers.LoanController;
import com.makowey.exceptions.LoanNotFoundException;
import com.makowey.model.Interest;
import com.makowey.model.loans.FlexibleLoan;
import com.makowey.model.loans.StandardLoan;
import com.makowey.repository.InterestRepository;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class InterestApplicationTests {

    @Autowired
    InterestRepository interestRepository;

    @Autowired
    LoanController loanController;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testStandardInterest() {
        Interest interest = loanController.getLoanById(1L);
        assertEquals(0.035, interest.getValue(), 0.0d);
    }

    @Test
    public void testAllInterests() {
        Long[] interestIds = new Long[]{1L, 2L};
        assertEquals(loanController.getAll().stream()
                        .map(Interest::getId)
                        .collect(Collectors.toList()),
                Arrays.asList(interestIds));
    }

    @Test
    public void testByName() {
        String type = "STANDARD";
        assertEquals(loanController.getLoanDetailsByName(type).getType(), type);
    }

    @Test
    public void testStandardCalculateMonthlyPaybackPlan() {
        Interest interest = loanController.getLoanById(1L);
        StandardLoan standardLoan = new StandardLoan(25000, interest, 20);

        assertEquals(145.00d, standardLoan.calculateMonthlyPaybackPlan().get(0).payment(), 0.01d);
    }

    @Test
    public void testFlexibleInterest() {
        Interest interest = loanController.getLoanById(2L);
        assertEquals(0.028d, interest.getValue(), 0.01d);
    }

    @Test
    public void testFlexibleCalculateMonthlyPaybackPlan() {
        Interest interest = loanController.getLoanById(2L);
        FlexibleLoan flexibleLoan = new FlexibleLoan(22200, interest, 11);

        thrown.expect(LoanNotFoundException.class);
        thrown.expectMessage("There is no calculateMonthlyPaybackPlan in place for FLEXIBLE!!!");
        assertEquals(125.00d, flexibleLoan.calculateMonthlyPaybackPlan().get(0).payment(), 0.01d);
    }
}
